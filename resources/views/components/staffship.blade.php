<span
    class="badge-font border border-danger fw-bold ms-1 px-2 rounded-pill small text-{{ $background === 'dark' ? 'white' : 'dark' }} text-capitalize"
    title="Feature Release Label: Staff Ship"
>
    Staff Ship
</span>

